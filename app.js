'use strict';

const express = require('express'),
    path = require('path'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    mongoose = require('mongoose');

const index = require('./routes/index'),
    session = require('express-session');
  //  config = require('./backend/Config').config;

var app = express();


app.set('views', path.join(__dirname, 'views'));
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));
// Muestra un log de todos los request en la consola
app.use(logger('dev'));
// Permite cambiar el HTML con el método POST
app.use(bodyParser());
// Simula DELETE y PUT
app.use(methodOverride());

// Use native Node promises
mongoose.Promise = global.Promise;

// connect to MongoDB
mongoose.connect('mongodb://localhost/TFG')
    .then(() =>  console.log('connection succesful'))
    .catch((err) => console.error(err));

app.use('/', index);

app.use(function (req, res) {
    if (req.session) {
        res.render('pageError', {msg: 'Page not valid'});
    } else {
        res.redirect('/');
    }
} );


app.set('port', (process.env.PORT || 3000));
app.listen(app.get('port'), () => {
    console.log('Server started on port '+app.get('port'));
});