// Load mongoose package
var mongoose = require('mongoose');

// Create a schema
var user = new mongoose.Schema({
    mail: String,
    password_app: String,
    salt: String,
    password_openstack: String,
});

module.exports = mongoose.model('users', user);