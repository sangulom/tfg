'use strict';

const express = require('express'),
    router = express.Router(),
    path = require('path'),
    fs = require('fs');
var Todo = require('../models/user.js');

const allowed_roles = ['admin'];

router.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '../views', 'index.html'));
});

router.get('/api/todos', function(req, res) {

    Todo.find(function (err, todos) {
        if (err) return next(err);
        console.log(todos);
        res.json(todos);
    });

});


module.exports = router;